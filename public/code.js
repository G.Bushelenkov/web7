$(document).ready(function() {
    $('.sl').slick({

        dots: true,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 4,

        responsive: [{
                breakpoint: 3840,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }

        ]


    });
});